# SASS - Basic Structure of Directory 
```
scss/
|
|-- base/              
|   |-- _all.scss         # Importa todas as mixins, variáveis e tipografias
|   |-- _mixin.scss       # Funções em scss
|   |-- _variables.scss   # Variáveis Globais
|   |-- _typography.scss  # Estilização de listas e conteúdos escritos
|   |-- _general.scss     # Estilização de macro-elementos e configurações prioritárias(ex: html, body, a, p)
|   ...
|
|-- layouts/              
|   |-- _footer.sass      # Toda a estilização da Footer
|   |-- _header.scss      # Toda a estilização da Header
|   |-- _sections.scss    # Toda estilização de contents
|   ...
|
|-- modules/              # Individual site components
|   |-- _blocks.scss      # Estilização de blocos para Flat Design
|   |-- _buttons.scss     # Estilização de botões
|   |-- _forms.scss       # Estilização de formulários
|   |-- _nav.scss         # Estilização da barra de navegação do site
|   |-- _jumbotron.scss   # Estilização de Jumbrotron
|   |-- _carousel.scss    # Estilização de slideshow/carousel
|   ...
|
`-- main.scss             # primary Sass file
```
